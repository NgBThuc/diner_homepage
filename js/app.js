// #### OVERLAY TOGGLE ####
const overlayToggleOpen = document.querySelector(".js-overlay-btn-open");
const overlayToggleClose = document.querySelector(".js-overlay-btn-close");
const overlayEl = document.querySelector(".js-overlay");

overlayToggleOpen.addEventListener("click", () => {
  overlayEl.dataset.toggle = "on";
});

overlayToggleClose.addEventListener("click", () => {
  overlayEl.dataset.toggle = "off";
});

document.addEventListener("keydown", (event) => {
  if (event.code === "Escape") {
    overlayEl.dataset.toggle = "off";
  }
});

// #### COUNTUP JS ####
$(".count-number").countUp();

// #### POPUP VIDEO ####
$(".video__popupButton").magnificPopup({
  type: "inline",
  fixedContentPos: true,
  fixedBgPos: true,
  overflowY: "auto",
  closeBtnInside: true,
  closeOnBgClick: true,
  enableEscapeKey: true,
  preloader: false,
  midClick: true,
  removalDelay: 100,
});

// #### OWL CAROUSEL ####

$(".owl-carousel").owlCarousel({
  center: true,
  items: 2,
  loop: true,
  margin: 10,
  stagePadding: 1,
  responsive: {
    0: {
      items: 1,
    },
  },
});

// #### DARK MODE ####
const storageKey = "user-color-scheme";
const colorModeKey = "--color-mode";

const modeToggleButton = document.querySelector(".js-mode-toggle");

// This function is used to take the color mode value from CSS.
const getCSSCustomProp = (propKey) => {
  let response = getComputedStyle(document.documentElement).getPropertyValue(
    propKey
  );
  // This trims all white space or unneeded character. We just need "dark" and "light".
  if (response.length) {
    response = response.replace(/\"/g, "").trim();
  }
  return response;
};

// This function is used to toggle between dark and light modes, and it is only toggled when the user presses the button.
const toggleSetting = () => {
  // Check the local storage if we have stored any modes.
  let currentMode = localStorage.getItem(storageKey);
  switch (currentMode) {
    // If we don't have any mode, we are going to take it from CSS custom properties.
    case null:
      currentMode =
        getCSSCustomProp(colorModeKey) === "dark" ? "light" : "dark";
      break;
    // If the stored mode is light, the current mode will be dark.
    case "light":
      currentMode = "dark";
      break;
    // If the stored mode is dark, the current mode will be light.
    case "dark":
      currentMode = "light";
      break;
  }
  // Save to local storage the current mode.
  localStorage.setItem(storageKey, currentMode);
  return currentMode;
};

// This function will apply the current mode to the user.
const applyMode = (mode) => {
  let currentMode = mode || localStorage.getItem(storageKey);
  if (currentMode) {
    document.documentElement.setAttribute(
      "data-user-color-scheme",
      currentMode
    );
  } else {
    document.documentElement.setAttribute(
      "data-user-color-scheme",
      getCSSCustomProp(colorModeKey)
    );
  }
};

// Add a mode toggle feature to the button.
modeToggleButton.addEventListener("click", (event) => {
  event.preventDefault();
  applyMode(toggleSetting());
});

applyMode();
